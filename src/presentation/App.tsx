import './App.css';

export default function App() {
  return (
    <div className="container">
      <h1 className="heading">React Webpack & Typescript</h1>
      <img className="logo" src="icon.svg" alt="" />
    </div>
  );
}
