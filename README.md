# Webpack React con TypeScript

Esta configuracion de Webpack está creada para facilitar el desarrollo y la construcción de aplicaciones React utilizando TypeScript. Incluye configuraciones para ESLint, Prettier, y MSW (Mock Service Worker).

## Estructura del Proyecto

La estructura básica del proyecto incluye:

- `src/`: código fuente de la aplicación.
  - `application/`: lógica de negocio.
  - `domain/`: definiciones de tipos y modelos.
  - `infrastructure/`: configuración de API y servicios externos.
  - `interfaces/`: componentes de UI y hooks.
- `public/`: archivos estáticos y `index.html`.
- `webpack/`: configuraciones de Webpack para desarrollo y producción.

## Configuración de Webpack

Incluye configuraciones para desarrollo y producción, gestionando transpilación de TypeScript, resolución de módulos y más.

### Aliases

Los paths están configurados en `tsconfig.json` y sincronizados con Webpack para facilitar los imports, utilizando alias como `@src`, `@interfaces`, entre otros.

## ESLint y Prettier

Configurados para garantizar un código limpio y consistente. Incluye reglas específicas para trabajar con React y TypeScript.

## Mock Service Worker

Preparado para integrar MSW para simular la API durante el desarrollo y testing, mejorando la fiabilidad de los tests.

## Uso

- Para iniciar el servidor de desarrollo: `yarn start`
- Para construir la versión de producción: `yarn run build`

## Tests

Configurado con Jest y MSW para realizar pruebas unitarias y de integración.

### Configuración

Se incluyen ejemplos de configuración de ESLint en `.eslintrc.json` y de MSW para ser utilizados tanto en desarrollo como en pruebas.


